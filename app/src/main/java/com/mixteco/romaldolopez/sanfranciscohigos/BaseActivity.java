package com.mixteco.romaldolopez.sanfranciscohigos;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import hugo.weaving.DebugLog;

public class BaseActivity extends AppCompatActivity implements FirebaseAuth.AuthStateListener {

    private AuthenticationFacade authFacade;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        authFacade = new AuthenticationFacade();
        authFacade.addAuthStateListener(this);
    }

    public void navigateToLoginTemplate() {
        authFacade.navigateToLoginTemplate(this);
    }



    @DebugLog
    protected boolean isLoggedIn() {
        return authFacade.isLoggedIn();
    }

    @DebugLog
    public FirebaseUser getCurrentUser() {
        return authFacade.getCurrentUser();
    }

    public Task<Void> signOut(@NonNull Context context) {
        return authFacade.signOut(context);
    }

    @Override
    protected void onDestroy() {
        authFacade.removeAuthStateListener(this);
        super.onDestroy();
    }

    @DebugLog
    @Override
    public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
        Log.d("AuthStateListener", "Auth state change isLogged in? " + isLoggedIn());
    }
}
