package com.mixteco.romaldolopez.sanfranciscohigos;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.ProgressBar;

import java.util.ArrayList;
import java.util.List;

public class Activity_groups extends AppCompatActivity {
    GroupAdapter adapter;
    List<group> mGroups = new ArrayList<group>();
    ProgressBar progressBar;
    @SuppressLint("RestrictedApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_groups);
        getSupportActionBar().setTitle("Grupos");
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);

        String Videopath1 = "https://firebasestorage.googleapis.com/v0/b/higosweb.appspot.com/o/videosGroups%2FLosCompasDe%20Higos.mp4?alt=media&token=7b23b0ef-adb3-4be7-8ad6-c5bb750eda74";
        String Videopath2 = "https://firebasestorage.googleapis.com/v0/b/higosweb.appspot.com/o/videosGroups%2FLosEegidosdeAsis.mp4?alt=media&token=3ab280cd-10be-4757-9377-50ce052ea5c0";
        String Videopath3 = "https://firebasestorage.googleapis.com/v0/b/higosweb.appspot.com/o/videosGroups%2FTDO.mp4?alt=media&token=b58fe128-cf75-45d3-98e0-6ba0b465f57d";
        String Videopath4 = "https://firebasestorage.googleapis.com/v0/b/higosweb.appspot.com/o/videosGroups%2FclubSFH.mp4?alt=media&token=213102e2-d014-4d90-b737-d2ad52334c59";
        String Videopath5 = "https://firebasestorage.googleapis.com/v0/b/higosweb.appspot.com/o/videosGroups%2FbandaTA.mp4?alt=media&token=970f9a00-5bda-49ca-a513-c18d6cb36d38";


        /* data to populate the RecyclerView with */
        addItem("Los Compas de Higos",Videopath1,"Oxnard, CA","Sergio Zuniga (805)827-5258", null, "https://www.youtube.com/channel/UC5AjQPmcO60qDpd6bB9q-Og");
        addItem("Los Elegidos de Asis", Videopath2, "Oxnard, CA", null,"https://www.facebook.com/jovany.morales.777", "https://www.youtube.com/user/elegidos805");
        addItem("Traviezos de Oaxaca", Videopath3, "San Francisco Higos, Oaxaca","Diego Lopez", null,"https://www.youtube.com/channel/UCiix5lJmOh4_fFHiAo--1Tw");
        addItem("Club San Francisco Higos", Videopath4, "Oxnard, CA", null, null,null);
        addItem("San Francisco Higos Inc.",null, "Oxnard, CA",null,"https://www.facebook.com/SanFranciscoHigosInc/",null );
        addItem("Banda Tierra Azul",Videopath5, "Oxnard, CA","Fernando Solano (805)616-3492",null,null );

        // set up the RecyclerView
        RecyclerView recyclerView = findViewById(R.id.view_group);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new GroupAdapter(this, mGroups);
        recyclerView.setAdapter(adapter);
    }
    public  void addItem(String Title, String Picture, String Location, String Contact, String FBLink, String YTLink) {
        mGroups.add(new group(Title, Picture, Location, Contact, FBLink, YTLink));
    }
}
