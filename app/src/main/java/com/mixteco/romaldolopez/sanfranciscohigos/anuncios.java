package com.mixteco.romaldolopez.sanfranciscohigos;

public class anuncios{
    private String Title;
    private String Description;
    private String Name;
    private String Email;



    public anuncios(String Title, String Description, String Name, String Email){

        this.Title = Title;
        this.Description = Description;
        this.Name = Name;
        this.Email = Email;

    }
    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }
    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public anuncios (){ }  //no argument constructor

}