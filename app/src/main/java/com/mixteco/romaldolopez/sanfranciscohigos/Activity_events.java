package com.mixteco.romaldolopez.sanfranciscohigos;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Canvas;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class Activity_events extends BaseActivity {

    public DatabaseError error;
    String Description;
    FirebaseDatabase database;
    DatabaseReference myRef;
    public static final String TAG ="asdf" ;
    FirebaseUser user;
    private RecyclerView mRecyclerView;
    private MyAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    List<event> mAllEvents = new ArrayList<event>();
    ArrayList<String> keyList = new ArrayList<>();
    SwipeController2 swipeController2;
    ProgressBar progressBar;
    RecyclerView.ViewHolder viewHolder;
    private static final int REQ_CODE = 900;
    SharedPreferences pref;
    SharedPreferences prefs;
    TextView mEmptyEvents;
    @SuppressLint("RestrictedApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_events);
        getSupportActionBar().setTitle("Eventos");
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        database=FirebaseDatabase.getInstance();
        myRef=database.getReference("events");
        prefs = PreferenceManager.getDefaultSharedPreferences(this);

        user = getCurrentUser();

        //user = FirebaseAuth.getInstance().getCurrentUser();
        pref = PreferenceManager.getDefaultSharedPreferences(this);
        progressBar = (ProgressBar) findViewById(R.id.simpleProgressBar3);
        retriveData(myRef);
        setupSwipe();

    }


    public void retriveData(DatabaseReference myRef){

    mRecyclerView = (RecyclerView) findViewById(R.id.listview_product);
    mEmptyEvents = (TextView) findViewById(R.id.emptyEvent);
    // use this setting to improve performance if you know that changes
    // in content do not change the layout size of the RecyclerView
    mRecyclerView.setHasFixedSize(true);

    // use a linear layout manager
    mLayoutManager = new LinearLayoutManager(this);
    mRecyclerView.setLayoutManager(mLayoutManager);

    progressBar.setVisibility(View.VISIBLE);
    myRef.addValueEventListener(new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            mAllEvents.clear();
            for (DataSnapshot postSnapshot: dataSnapshot.getChildren()) {

                keyList.add(postSnapshot.getKey());

                event data = postSnapshot.getValue(event.class);
                mAllEvents.add(data);

            }

            if(mAllEvents.isEmpty()){
                mRecyclerView.setVisibility(View.GONE);
                mEmptyEvents.setVisibility(View.VISIBLE);
            }
            else {
                mRecyclerView.setVisibility(View.VISIBLE);
                mEmptyEvents.setVisibility(View.GONE);
            }

            mAdapter =  new MyAdapter(getApplicationContext(), mAllEvents);
            //lvEvent.setAdapter(adapter);
            mRecyclerView.setAdapter(mAdapter);

            mAdapter.notifyDataSetChanged();
            progressBar.setVisibility(View.GONE);
            //lvEvent.setEmptyView(findViewById(R.id.emptyElement));

        }

        @Override
        public void onCancelled(DatabaseError databaseError) {
            Log.w(TAG, "Failed to read value.", error.toException());

        }

    });
    //progressBar.setVisibility(View.GONE);

}  // convenience method for getting data at click position


    private void setupSwipe() {

        swipeController2 = new SwipeController2(new SwipeControllerActions() {
            @Override
            public void onRightClicked(int position) {
                AlertDialog diaBox = AskOption(position);
                diaBox.show();
            }
        });


            ItemTouchHelper itemTouchhelper = new ItemTouchHelper(swipeController2);
            itemTouchhelper.attachToRecyclerView(mRecyclerView);

        mRecyclerView.addItemDecoration(new RecyclerView.ItemDecoration() {
            @Override
            public void onDraw(Canvas c, RecyclerView parent, RecyclerView.State state) {
                swipeController2.onDraw(c,getApplicationContext() );
                prefs.edit().putBoolean("IsUser", false).apply();

            }
        });
    }
    public void addButton(View view){
        //FirebaseUser currentUser = mAuth.getCurrentUser();
            Intent intent = new Intent(Activity_events.this, Activity_newevent.class);
            startActivity(intent);

    }
    private AlertDialog AskOption(final int position)
    {
        AlertDialog myQuittingDialogBox =new AlertDialog.Builder(this)
                //set message, title, and icon
                .setTitle("Borrar")
                .setMessage("Seguro que quieres borrar?")

                .setPositiveButton("Borrar", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int whichButton) {
                        //your deleting code
                        event item = (event) mAdapter.getItem(position);
                        if(item.getEmail().equals(user.getEmail())){
                            mAllEvents.remove(item);
                            // adapter.notifyDataSetChanged();
                            //new code below
                            myRef.child(keyList.get(position)).removeValue();
                            keyList.remove(position);
                            mAdapter.notifyDataSetChanged();
                        }else{
                            Toast.makeText(getApplicationContext(),"No eres el creador.",Toast.LENGTH_SHORT).show();

                        }
                        dialog.dismiss();
                    }

                })



                .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.dismiss();

                    }
                })
                .create();
        return myQuittingDialogBox;

    }
    public void showMap(Uri geoLocation) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse("geo:0,0?q=my+street+address")); //lat lng or address query
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }
    }
    // Get Login State
    public boolean isLoggedIn(){
        return pref.getBoolean("Islogin", false);
    }

}
