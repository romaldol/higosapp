package com.mixteco.romaldolopez.sanfranciscohigos;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.util.Linkify;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

public class Activity_groupDetails extends AppCompatActivity {
    String video_name="test";
    ProgressBar progressbar;
    Bundle extra;
    TextView videoMessg;
    TextView videoMessg2;

    @SuppressLint("RestrictedApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group_details);
        getSupportActionBar().setTitle("Detalles");
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);

        progressbar = findViewById(R.id.videload);


       extra = getIntent().getExtras();
        String Title = extra.getString("data_Title");
        TextView TvTitle = (TextView) findViewById(R.id.TVTitle);
        TvTitle.setText(Title);

        //Video



checkConnection();


       //Location
        String Location = extra.getString("data_Location");
        TextView location = (TextView) findViewById(R.id.tvLocation);
        location.setText(Location);



        //Contact
        String Contact = extra.getString("data_Contact");
        TextView contact = (TextView) findViewById(R.id.tvContact);
        contact.setAutoLinkMask(Linkify.PHONE_NUMBERS);
        if(Contact == null){
            String message =  "Contacto no esta disponible.";

            contact.setText(message);
        }else {
            contact.setText(Contact);
        }

        //Facebook
        final String FBLink = extra.getString("data_FBLink");
        Button fblink = (Button) findViewById(R.id.fbbtn);
        fblink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(FBLink == null){
                    Toast.makeText(getApplicationContext(),"Link no esta disponible.",Toast.LENGTH_SHORT).show();
                }else {
                    goToUrl(FBLink);
                }
            }
        });

        //Youtube
        final String YTLink = extra.getString("data_YTLink");
        Button ytlink = (Button) findViewById(R.id.uTube);
        ytlink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(YTLink==null){
                    Toast.makeText(getApplicationContext(),"Link no esta disponible.",Toast.LENGTH_SHORT).show();

                }else {
                    goToUrl(YTLink);
                }
            }
        });
    }

    public void goToSo (View view) {
        goToUrl ( "http://stackoverflow.com/");
    }

    public void goToSu (View view) {
        goToUrl ( "http://superuser.com/");
    }

    private void goToUrl (String url) {
        Uri uriUrl = Uri.parse(url);
        Intent launchBrowser = new Intent(Intent.ACTION_VIEW, uriUrl);
        startActivity(launchBrowser);
    }

    protected boolean isOnline() {

        ConnectivityManager cm = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo netInfo = cm.getActiveNetworkInfo();

        if (netInfo != null && netInfo.isConnectedOrConnecting()) {

            return true;

        } else {

            return false;

        }

    }

    public void checkConnection(){

        //Video
        VideoView videoView =(VideoView)findViewById(R.id.videoView2);
        Display display = getWindowManager().getDefaultDisplay();
        int width = display.getWidth();
        int height = display.getHeight();
        ViewGroup.LayoutParams params=videoView.getLayoutParams();
        params.width = width;
        params.height = height;
        videoView.setLayoutParams(params);

        String VidPath = extra.getString("data_uri");
        MediaController mediaController= new MediaController(this);
        mediaController.setAnchorView(videoView);

        if(isOnline()){


            if(VidPath == null){
               videoMessg = (TextView) findViewById(R.id.VideoMessg);
                videoView.setVisibility(View.GONE);
                videoMessg.setVisibility(View.VISIBLE);
                String videomess = "Video no esta disponible.";
                videoMessg.setText(videomess);
                progressbar.setVisibility(View.GONE);


            }else{
                Uri uri=Uri.parse(VidPath);
                videoView.setMediaController(mediaController);
                videoView.setVideoURI(uri);
                videoView.requestFocus();
                //videoView.start();

                progressbar.setVisibility(View.VISIBLE);
                videoView.setBackgroundColor(Color.WHITE); // Your color.
                videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {

                    @Override
                    public void onPrepared(MediaPlayer mp) {



                        mp.start();

                        mp.setOnVideoSizeChangedListener(new MediaPlayer.OnVideoSizeChangedListener() {

                            @Override
                            public void onVideoSizeChanged(MediaPlayer mp, int arg1, int arg2) {
                                // TODO Auto-generated method stub
                                //Log.e(TAG, "Changed");
                                videoView.setBackgroundColor(Color.TRANSPARENT);
                                progressbar.setVisibility(View.GONE);
                                mp.start();
                            }
                        });


                    }
                });


            }
        }else{
            videoMessg2 = (TextView) findViewById(R.id.VideoMessg2);

            videoMessg2.setVisibility(View.VISIBLE);
            String videomess = "No hay conexion de Internet";
            videoMessg2.setText(videomess);
            videoView.setBackgroundColor(Color.WHITE); // Your color.

        }

    }

}

