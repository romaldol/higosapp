package com.mixteco.romaldolopez.sanfranciscohigos;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Canvas;
import android.support.v7.app.AlertDialog;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class Activity_anuncios extends BaseActivity {
    FirebaseDatabase database;
    DatabaseReference myRef;
    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    ProgressBar progressBar;
    List<anuncios> mAllAnuncios = new ArrayList<anuncios>();
    ArrayList<String> keyList = new ArrayList<>();
    private AnunciosAdapter mAdapter;
    SwipeController swipeController;
    FirebaseUser user;
    TextView mEmptyView;
    @SuppressLint("RestrictedApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_anuncios);
        getSupportActionBar().setTitle("Anuncios");
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        user = getCurrentUser();
        database=FirebaseDatabase.getInstance();
        myRef=database.getReference("Anuncios");
        progressBar = (ProgressBar) findViewById(R.id.simpleProgressBar4);

        retriveData(myRef);
        setupSwipe();

    }

    public void retriveData(DatabaseReference myRef){

        mRecyclerView = (RecyclerView) findViewById(R.id.Rview_Anuncios);
        mEmptyView = (TextView) findViewById(R.id.emptyAnuncio);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        progressBar.setVisibility(View.VISIBLE);
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                mAllAnuncios.clear();
                for (DataSnapshot postSnapshot: dataSnapshot.getChildren()) {

                    keyList.add(postSnapshot.getKey());

                    anuncios data = postSnapshot.getValue(anuncios.class);
                    mAllAnuncios.add(data);

                }

                if(mAllAnuncios.isEmpty()){
                    mRecyclerView.setVisibility(View.GONE);
                    mEmptyView.setVisibility(View.VISIBLE);
                }
                else {
                    mRecyclerView.setVisibility(View.VISIBLE);
                    mEmptyView.setVisibility(View.GONE);
                }
                mAdapter =  new AnunciosAdapter(getApplicationContext(), mAllAnuncios);
                //lvEvent.setAdapter(adapter);
                mRecyclerView.setAdapter(mAdapter);

                mAdapter.notifyDataSetChanged();
                progressBar.setVisibility(View.GONE);
                //lvEvent.setEmptyView(findViewById(R.id.emptyElement));

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                //Log.w(TAG, "Failed to read value.", error.toException());

            }

        });
        //progressBar.setVisibility(View.GONE);

    }
    private void setupSwipe() {

        swipeController = new SwipeController(new SwipeControllerActions() {
            @Override
            public void onRightClicked(int position) {
                AlertDialog diaBox = AskOption(position);
                diaBox.show();
            }
        });


        ItemTouchHelper itemTouchhelper = new ItemTouchHelper(swipeController);
        itemTouchhelper.attachToRecyclerView(mRecyclerView);

        mRecyclerView.addItemDecoration(new RecyclerView.ItemDecoration() {
            @Override
            public void onDraw(Canvas c, RecyclerView parent, RecyclerView.State state) {
                swipeController.onDraw(c,getApplicationContext() );
            }
        });
    }
    private AlertDialog AskOption(final int position)
    {
        AlertDialog myQuittingDialogBox =new AlertDialog.Builder(this)
                //set message, title, and icon
                .setTitle("Borrar")
                .setMessage("Seguro que quieres borrar?")

                .setPositiveButton("Borrar", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int whichButton) {
                        //your deleting code
                        anuncios item = (anuncios) mAdapter.getItem(position);
                        if(item.getEmail().equals(user.getEmail())){
                            mAllAnuncios.remove(item);
                            // adapter.notifyDataSetChanged();
                            //new code below
                            myRef.child(keyList.get(position)).removeValue();
                            keyList.remove(position);
                            mAdapter.notifyDataSetChanged();
                        }else{
                            Toast.makeText(getApplicationContext(),"No eres el creador.",Toast.LENGTH_SHORT).show();

                        }
                        dialog.dismiss();
                    }

                })



                .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.dismiss();

                    }
                })
                .create();
        return myQuittingDialogBox;

    }
    public void addAnunciosButton(View view){
       // FirebaseUser currentUser = mAuth.getCurrentUser();
            Intent intent = new Intent(Activity_anuncios.this, Activity_newAnuncio.class);
            startActivity(intent);

    }
}
