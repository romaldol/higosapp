package com.mixteco.romaldolopez.sanfranciscohigos;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.List;

public class Activity_newAnuncio extends AppCompatActivity {
    EditText Title;
    EditText Description;
    private FirebaseDatabase database;
    private DatabaseReference myRef;
    List<anuncios> mAnuncioList = new ArrayList<>();
    FirebaseUser user;

    @SuppressLint("RestrictedApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_anuncio);
        getSupportActionBar().setTitle("Crear Anuncio");
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);


        database = FirebaseDatabase.getInstance();
        myRef = database.getReference("Anuncios");
        // AutoWrapFilter.applyAutoWrap(editText,10);
        user = FirebaseAuth.getInstance().getCurrentUser();
    }
    public void OnSubmit(View view) {
        String name = user.getDisplayName();
        String email = user.getEmail();
        Title = (EditText) findViewById(R.id.TitleAnu);
        Description = (EditText) findViewById(R.id.editText_DescriptionAnun);


        String title = Title.getText().toString();
        String description = Description.getText().toString();
        if(title.isEmpty()){
            Title.setError("Requerido");
            return;
        }else if(description.isEmpty()){
            Description.setError("Requerido");
            return;
        }

        addItem(title, description, name, email);

        for(anuncios data : mAnuncioList){
            myRef.push().setValue(data);
        }



        Intent intent = new Intent(Activity_newAnuncio.this, Activity_anuncios.class);
        startActivity(intent);
    }
    public  void addItem(String Title, String Description, String Name, String Email) {
        mAnuncioList.add(new anuncios(Title, Description, Name, Email));
    }


}
