package com.mixteco.romaldolopez.sanfranciscohigos;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

public class GroupAdapter extends  RecyclerView.Adapter<GroupAdapter.ViewHolder>{


    private List<group> mData;
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;
    private Context mContext;

    // data is passed into the constructor
    GroupAdapter(Context context, List<group> data) {
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
        this.mContext = context;

    }

    // inflates the row layout from xml when needed
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.item_group_list, parent, false);
        return new ViewHolder(view);
    }

    // binds the data to the TextView in each row
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        String Title = mData.get(position).getTitle();
        String Location = mData.get(position).getLocation();

        holder.myTitle.setText(Title);
        holder.myLocation.setText(Location);
    }

    // total number of rows
    @Override
    public int getItemCount() {
        return mData.size();
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView myTitle;
        TextView myLocation;
        TextView myContact;


        ViewHolder(View itemView) {
            super(itemView);
           myTitle = itemView.findViewById(R.id.GroupTitle);
           myLocation = itemView.findViewById(R.id.GroupLocation);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int position = getAdapterPosition();
            //event feedItem = mData.get(position);

            Intent intent = new Intent(mContext, Activity_groupDetails.class);
            intent.putExtra("data_Title",  mData.get(position).getTitle());
            intent.putExtra("data_uri", mData.get(position).getPicture());
            intent.putExtra("data_Location", mData.get(position).getLocation());
            intent.putExtra("data_Contact", mData.get(position).getContact());
            intent.putExtra("data_FBLink", mData.get(position).getFBLink());
            intent.putExtra("data_YTLink", mData.get(position).getYTLink());

            mContext.startActivity(intent);
        }
    }

    // convenience method for getting data at click position
    group getItem(int id) {
        return mData.get(id);
    }

    // allows clicks events to be caught
    void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }

}
