package com.mixteco.romaldolopez.sanfranciscohigos;

@SuppressWarnings("serial") //With this annotation we are going to hide compiler warnings
public class event{
    private int id;
    private String Title;
    private String Time;
    private String Location;
    private String User;
    private String Email;
    private String Date;




    public event(int id, String Title, String Time, String Location, String Date, String User, String Email){
        this.User = User;
        this.id = id;
        this.Title = Title;
        this.Time = Time;
        this.Location = Location;
        this.Email = Email;
        this.Date = Date;


    }
    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        Date = date;
    }
    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public void setUser(String User) {
        this.User = User;
    }
    public int getId() {
        return id;
    }
    public String getUser() {
        return User;
    }
    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String Title) {
        this.Title = Title;
    }

    public String getTime() {
        return Time;
    }

    public void setTime(String Time) {
        this.Time = Time;
    }

    public String getLocation() {
        return Location;
    }

    public void setLocation(String Location) {
        this.Location = Location;
    }

    public event (){ }  //no argument constructor

}
