package com.mixteco.romaldolopez.sanfranciscohigos;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

public class MainActivity extends BaseActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    ProgressBar progressBar;
    ViewPager viewPager;
    Context context;
    SharedPreferences prefs;
    NavigationView navigationView2;
    NavigationView navigationView3;
    DrawerLayout drawerLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        prefs = PreferenceManager.getDefaultSharedPreferences(this);


        //add this line to display menu1 when the activity is loaded

        viewPager = (ViewPager) findViewById(R.id.viewPager_1);
        Display display = getWindowManager().getDefaultDisplay();
        int width = display.getWidth();
        int height = display.getHeight();
        ViewGroup.LayoutParams params = viewPager.getLayoutParams();
        params.width = width;
        params.height = height;
        viewPager.setLayoutParams(params);
        ViewPageAdapter viewPageAdapter = new ViewPageAdapter(this);
        viewPager.setAdapter(viewPageAdapter);


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);


// The `DrawerListener` allows us to update the Navigation Menu.
        drawer.addDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
            }

            @Override
            public void onDrawerOpened(View drawerView) {
            }

            @Override
            public void onDrawerClosed(View drawerView) {
            }

            @Override
            public void onDrawerStateChanged(int newState) {
                // Update the back and forward menu items every time the drawer opens.
                if (isLoggedIn()) {
                    UnhideItem();
                } else {
                    hideItem();
                }
            }
        });

    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem signOut = menu.findItem(R.id.action_signout);
        MenuItem signIn = menu.findItem(R.id.action_signin);

        if (isLoggedIn()) {
            signOut.setVisible(true);
            signIn.setVisible(false);
        } else {
            signOut.setVisible(false);
            signIn.setVisible(true);
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_signin) {
            navigateToLoginTemplate();
        }
        if (id == R.id.action_signout) {
            signOut(this)
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            Toast.makeText(MainActivity.this, "Your Sign Out!", Toast.LENGTH_SHORT).show();
                        }
                    });
            //Toast.makeText(MainActivity.this, "user Sign OUt!", Toast.LENGTH_SHORT).show();

        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            // Handle the camera action
        } else if (id == R.id.nav_events) {
            Intent i = new Intent(MainActivity.this, Activity_events.class);
            startActivity(i);
        } else if (id == R.id.nav_Groups) {
            Intent i = new Intent(MainActivity.this, Activity_groups.class);
            startActivity(i);
        } else if (id == R.id.nav_Anuncios) {
            Intent i = new Intent(MainActivity.this, Activity_anuncios.class);
            startActivity(i);
        } else if (id == R.id.nav_facebook) {
            Intent i = new Intent(MainActivity.this, Activity_facebook.class);
            startActivity(i);
        } else if (id == R.id.nav_youtube) {
            Intent i = new Intent(MainActivity.this, Activity_youtube.class);
            startActivity(i);
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    private void hideItem() {
        navigationView2 = (NavigationView) findViewById(R.id.nav_view);
        Menu nav_Menu = navigationView2.getMenu();

        nav_Menu.findItem(R.id.nav_events).setVisible(false);
        nav_Menu.findItem(R.id.nav_Anuncios).setVisible(false);
    }

    private void UnhideItem() {
        navigationView2 = (NavigationView) findViewById(R.id.nav_view);
        Menu nav_Menu = navigationView2.getMenu();

        nav_Menu.findItem(R.id.nav_events).setVisible(true);
        nav_Menu.findItem(R.id.nav_Anuncios).setVisible(true);
    }
}
