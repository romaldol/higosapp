package com.mixteco.romaldolopez.sanfranciscohigos;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.firebase.ui.auth.AuthUI;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.Arrays;
import java.util.List;

public class AuthenticationFacade {

    private static final String TAG = AuthenticationFacade.class.getSimpleName();
    public static final int RC_SIGN_IN = 900;
    private FirebaseAuth mFirebaseAuth = FirebaseAuth.getInstance();


    public void addAuthStateListener(@NonNull FirebaseAuth.AuthStateListener listener) {
        mFirebaseAuth.addAuthStateListener(listener);
    }

    public void removeAuthStateListener(@NonNull FirebaseAuth.AuthStateListener listener) {
        mFirebaseAuth.removeAuthStateListener(listener);
    }

    public FirebaseUser getCurrentUser() {
        if (isLoggedIn()) {
            return mFirebaseAuth.getCurrentUser();
        }
        Log.d(TAG, "User is not logged in returning null");
        return null;
    }

    public boolean isLoggedIn() {
        return mFirebaseAuth.getCurrentUser() != null;
    }

    public void navigateToLoginTemplate(@NonNull final AppCompatActivity activity) {
        activity.startActivityForResult(
                AuthUI.getInstance()
                        .createSignInIntentBuilder()
                        .setAvailableProviders(getAuthProviders())
                        .build(), RC_SIGN_IN);
    }

    private List<AuthUI.IdpConfig> getAuthProviders() {
        return Arrays.asList(
                new AuthUI.IdpConfig.EmailBuilder().build(),
                new AuthUI.IdpConfig.GoogleBuilder().build());
    }

    public Task<Void> signOut(@NonNull Context context) {
        return AuthUI.getInstance().signOut(context);
    }
}
