package com.mixteco.romaldolopez.sanfranciscohigos;

import android.annotation.SuppressLint;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class Activity_anunciosDetails extends AppCompatActivity {

    @SuppressLint("RestrictedApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_anuncios_details);
        getSupportActionBar().setTitle("Detalles");
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);


        Bundle extra = getIntent().getExtras();

        String Title = extra.getString("data_Title");
        TextView TvTitle = (TextView) findViewById(R.id.Anuncios_title);
        TvTitle.setText(Title);


        String Description = extra.getString("data_Description");
        TextView description = (TextView) findViewById(R.id.Anuncios_Description);
        description.setText(Description);

        String Auth = extra.getString("data_Auth");
        TextView auth = (TextView) findViewById(R.id.AuthAnuncio);
        auth.setText(Auth);
    }
}
