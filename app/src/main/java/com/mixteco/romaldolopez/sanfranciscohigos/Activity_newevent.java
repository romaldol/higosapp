package com.mixteco.romaldolopez.sanfranciscohigos;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TimePicker;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class Activity_newevent extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
 private EditText Title;
 private EditText Time;
 private EditText Location;
 Button btn;
 private FirebaseDatabase database;
private DatabaseReference myRef;
private static int idCounter = 0 ;
List<event> mEventList = new ArrayList<>();
    FirebaseUser user;
    Button eButtonTime;
    Button eButtonTime2;

    Button eButtonDate;
    Button mLocationBtn;
    private String format = "";
    Calendar mcurrentTime;
    int hour;
    int minute;
    Calendar mcurrentTime2;
    int hour2;
    int minute2;
    Calendar mcurrentDate;
    int mYear;
    int mMonth;
    int mDay;
    String placeAddress;
    // Constants
    public static final String TAG = Activity_newevent.class.getSimpleName();
    private static final int PERMISSIONS_REQUEST_FINE_LOCATION = 111;
    private static final int PLACE_PICKER_REQUEST = 1;
    String date;
    String time;
    // Member variables
    private GoogleApiClient mClient;
    //private Geofencing mGeofencing;

    @SuppressLint("RestrictedApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_newevent);
        getSupportActionBar().setTitle("Crear Evento");
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
       database = FirebaseDatabase.getInstance();
        myRef = database.getReference("events");
         user = FirebaseAuth.getInstance().getCurrentUser();

         eButtonDate = (Button) findViewById(R.id.SetDate);
        eButtonTime= (Button) findViewById(R.id.SetTime);
        eButtonTime2 = (Button) findViewById(R.id.SetTime2);

        mLocationBtn = (Button) findViewById(R.id.locationBtn);



        mClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .addApi(Places.GEO_DATA_API)
                .enableAutoManage(this, this)
                .build();


    }
    public  void addItem(int ID, String title, String time, String location,String date, String user, String email) {
        mEventList.add(new event(ID, title, time, location,date,user, email));
    }

    public  List<event> getData() {
        return mEventList;
    }


    public void onClickSubmit(View view) {
        idCounter += 1;
        Title = (EditText) findViewById(R.id.Title);
        SimpleDateFormat mSDF = new SimpleDateFormat("hh:mm a", Locale.US );
        //time = mSDF.format(mcurrentTime.getTime());

        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
        //date = sdf.format(mcurrentDate.getTime());

        String name = user.getDisplayName();
        String email = user.getEmail();
        String title = Title.getText().toString();

        if(title.isEmpty()){
            Title.setError("Requerido");
            return;
        }else if(placeAddress==null){
            mLocationBtn.setError("Requerido");
            return;
        }
        try
        {
            date = sdf.format(mcurrentDate.getTime());

        }
        catch(Exception e)
        {
            eButtonDate.setError("Requerido");
            return;
        }
        try
        {
            time = mSDF.format(mcurrentTime.getTime()) + " - " + mSDF.format(mcurrentTime2.getTime());
        }
        catch(Exception e)
        {
            eButtonTime.setError("Requerido");
            eButtonTime2.setError("Requerido");
            return;
        }


        addItem(idCounter, title, time, placeAddress, date,name,email);

        for(event data : mEventList){
            myRef.push().setValue(data);
        }



        Intent intent = new Intent(Activity_newevent.this, Activity_events.class);
        startActivity(intent);
    }
public void onClickDate(View view){
    // TODO Auto-generated method stub
    //To show current date in the datepicker
    mcurrentDate = Calendar.getInstance();
    mYear = mcurrentDate.get(Calendar.YEAR);
    mMonth = mcurrentDate.get(Calendar.MONTH);
    mDay = mcurrentDate.get(Calendar.DAY_OF_MONTH);

    DatePickerDialog mDatePicker;
    mDatePicker = new DatePickerDialog(Activity_newevent.this, new DatePickerDialog.OnDateSetListener() {
        public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
            // TODO Auto-generated method stub
            /*      Your code   to get date and time    */
            selectedmonth = selectedmonth + 1;
            mcurrentDate.set(Calendar.YEAR,selectedyear);
            mcurrentDate.set(Calendar.MONTH,selectedmonth);
            mcurrentDate.set(Calendar.DAY_OF_MONTH,selectedday);

            eButtonDate.setText("" + selectedmonth+ "/" + selectedday + "/" + selectedyear);
        }
    }, mYear, mMonth, mDay);
    mDatePicker.setTitle("Select Date");
    mDatePicker.show();
}
public void onClickTime(View view){
    mcurrentTime = Calendar.getInstance();
    hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
    minute = mcurrentTime.get(Calendar.MINUTE);

    TimePickerDialog mTimePicker;
    mTimePicker = new TimePickerDialog(Activity_newevent.this, new TimePickerDialog.OnTimeSetListener() {
        @Override
        public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
            mcurrentTime.set(Calendar.HOUR_OF_DAY, selectedHour);
            mcurrentTime.set(Calendar.MINUTE, selectedMinute);
            if (selectedHour == 0) {
                selectedHour += 12;
                format = "AM";
            } else if (selectedHour == 12) {
                format = "PM";
            } else if (selectedHour > 12) {
                selectedHour -= 12;
                format = "PM";
            } else {
                format = "AM";
            }


            eButtonTime.setText(selectedHour + ":" + selectedMinute +" "+ format);
            //scheduleTime.setText(selectedHour + ":" + selectedMinute);
            //scheduleTime.setText( strHrsToShow+":"+datetime.get(Calendar.MINUTE)+" "+am_pm );
        }
    }, hour, minute+30, false);//Yes 24 hour time
    mTimePicker.setTitle("Select Time");
    mTimePicker.show();
}
    public void onClickTime2(View view){
        mcurrentTime2 = Calendar.getInstance();
        hour2 = mcurrentTime2.get(Calendar.HOUR_OF_DAY);
        minute2 = mcurrentTime2.get(Calendar.MINUTE);

        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(Activity_newevent.this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                mcurrentTime2.set(Calendar.HOUR_OF_DAY, selectedHour);
                mcurrentTime2.set(Calendar.MINUTE, selectedMinute);
                if (selectedHour == 0) {
                    selectedHour += 12;
                    format = "AM";
                } else if (selectedHour == 12) {
                    format = "PM";
                } else if (selectedHour > 12) {
                    selectedHour -= 12;
                    format = "PM";
                } else {
                    format = "AM";
                }


                eButtonTime2.setText(selectedHour + ":" + selectedMinute +" "+ format);
                //scheduleTime.setText(selectedHour + ":" + selectedMinute);
                //scheduleTime.setText( strHrsToShow+":"+datetime.get(Calendar.MINUTE)+" "+am_pm );
            }
        }, hour2, minute2+30, false);//Yes 24 hour time
        mTimePicker.setTitle("Select Time");
        mTimePicker.show();
    }
    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Log.i(TAG, "API Client Connection Successful!");
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.i(TAG, "API Client Connection Suspended!");

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.e(TAG, "API Client Connection Failed!");

    }


    public void onAddPlaceButtonClicked(View view) {

        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(Activity_newevent.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);

            return;
        }

        try {

            PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
            Intent i = builder.build(this);
            startActivityForResult(i, PLACE_PICKER_REQUEST);
        } catch (GooglePlayServicesRepairableException e) {
            Log.e(TAG, String.format("GooglePlayServices Not Available [%s]", e.getMessage()));
        } catch (GooglePlayServicesNotAvailableException e) {
            Log.e(TAG, String.format("GooglePlayServices Not Available [%s]", e.getMessage()));
        } catch (Exception e) {
            Log.e(TAG, String.format("PlacePicker Exception: %s", e.getMessage()));
        }
    }
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_PICKER_REQUEST && resultCode == RESULT_OK) {
            Place place = PlacePicker.getPlace(this, data);
            if (place == null) {
                Log.i(TAG, "No place selected");
                return;
            }

            String placeID = place.getId();
            CharSequence placee = place.getAddress();
            placeAddress = placee.toString();
            mLocationBtn.setText(placeAddress);


        }
    }
}
