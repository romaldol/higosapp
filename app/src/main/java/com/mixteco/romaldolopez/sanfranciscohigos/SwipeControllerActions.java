package com.mixteco.romaldolopez.sanfranciscohigos;

public abstract class SwipeControllerActions {

    public void onLeftClicked(int position) {}

    public void onRightClicked(int position) {}

}