package com.mixteco.romaldolopez.sanfranciscohigos;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.util.Linkify;
import android.view.View;
import android.widget.TextView;

public class Activity_eventDetails extends AppCompatActivity {
    String Address;
    @SuppressLint("RestrictedApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_details);
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Detalles");

        Bundle extra = getIntent().getExtras();
        String Title = extra.getString("data_Title");
        TextView title = (TextView) findViewById(R.id.textViewTitle);
        title.setText(Title);

        Address = extra.getString("data_Location");
        TextView maddress = (TextView) findViewById(R.id.Address);
        maddress.setText(Address);
        Linkify.addLinks(maddress , Linkify.MAP_ADDRESSES);


       // Bundle extra = getIntent().getExtras();
        String Hora = extra.getString("data_Time");
        TextView hora = (TextView) findViewById(R.id.Hora);
        hora.setText(Hora);

        // Bundle extra = getIntent().getExtras();
        String Fecha = extra.getString("data_Date");
        TextView fecha = (TextView) findViewById(R.id.Fecha);
        fecha.setText(Fecha);

        // Bundle extra = getIntent().getExtras();
        String Author = extra.getString("data_User");
        TextView author = (TextView) findViewById(R.id.Author);
        author.setText(Author);


        maddress.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v) {
                try {
                    Address = Address.replace(' ', '+');
                    Intent geoIntent = new Intent (android.content.Intent.ACTION_VIEW, Uri.parse ("geo:0,0?q=" + Address)); // Prepare intent
                    startActivity(geoIntent); // Initiate lookup
                } catch (Exception e){
                }
            }
        });


    }

}
