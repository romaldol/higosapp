package com.mixteco.romaldolopez.sanfranciscohigos;

public class group {
    private String Title;
    private String Picture;
    private String Location;
    private String Contact;
    private String FBLink;
    private String YTLink;



    public group(String Title, String Picture, String Location, String Contact, String FBLink, String YTLink){

        this.Title = Title;
        this.Picture=Picture;
        this.Location = Location;
        this.Contact=Contact;
        this.FBLink = FBLink;
        this.YTLink = YTLink;


    }
    public String getFBLink() {
        return FBLink;
    }

    public void setFBLink(String FBLink) {
        this.FBLink = FBLink;
    }

    public String getYTLink() {
        return YTLink;
    }

    public void setYTLink(String YTLink) {
        this.YTLink = YTLink;
    }
    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getPicture() {
        return Picture;
    }

    public void setPicture(String picture) {
        Picture = picture;
    }

    public String getLocation() {
        return Location;
    }

    public void setLocation(String location) {
        Location = location;
    }

    public String getContact() {
        return Contact;
    }

    public void setContact(String contact) {
        Contact = contact;
    }


}
