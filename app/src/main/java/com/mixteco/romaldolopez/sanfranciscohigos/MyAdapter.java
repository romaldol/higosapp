package com.mixteco.romaldolopez.sanfranciscohigos;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder> {
    private static List<event> mData;
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;
    private Context mContext;

    // data is passed into the constructor
    MyAdapter(Context context, List<event> data) {
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
        this.mContext = context;

    }

    // inflates the row layout from xml when needed
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.item_event_list, parent, false);
        return new ViewHolder(view);
    }

    // binds the data to the TextView in each row
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
      String Title = mData.get(position).getTitle();
        String Date = mData.get(position).getDate();
    /*    String User = mData.get(position).getUser();
        String Location = mData.get(position).getLocation();
        String Time = mData.get(position).getTime();
        */

        holder.tvTitle.setText(Title);
        holder.tvDate.setText("Fecha: "+Date);
        /*holder.tvUser.setText(User);
        holder.tvLocation.setText("Direccion "+Location);
        holder.tvTime.setText("Hora: "+Time);
        */


    }

    // total number of rows
    @Override
    public int getItemCount() {
        if(mData == null){
            return 0;
        }else {
            return mData.size();
        }
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView tvTitle;
        TextView tvDate;
     /*   TextView tvUser;
        TextView tvLocation;
        TextView tvTime;
        */

        ViewHolder(View itemView) {
            super(itemView);
            tvTitle = itemView.findViewById(R.id.Title);
            tvDate = itemView.findViewById(R.id.Date);
           /* tvUser = itemView.findViewById(R.id.User);
            tvLocation = itemView.findViewById(R.id.Location);
            tvTime = itemView.findViewById(R.id.Time);
            */

            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View view) {
            int position = getAdapterPosition();
            //event feedItem = mData.get(position);

                Intent intent = new Intent(mContext, Activity_eventDetails.class);
                intent.putExtra("data_Title",  mData.get(position).getTitle());
                intent.putExtra("data_User", mData.get(position).getUser());
                intent.putExtra("data_Location", mData.get(position).getLocation());
                intent.putExtra("data_Time", mData.get(position).getTime());
                intent.putExtra("data_Date",mData.get(position).getDate());

                mContext.startActivity(intent);
                Toast.makeText(view.getContext(), "ITEM PRESSED = " + String.valueOf(getAdapterPosition()), Toast.LENGTH_SHORT).show();

        }
    }

    // convenience method for getting data at click position
    public static Object getItem(int id) {
        return mData.get(id);
    }

    // allows clicks events to be caught
    void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);

    }
}